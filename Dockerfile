FROM node:16-alpine AS deps
WORKDIR /app
COPY yarn.lock /app/
COPY package.json /app/
RUN yarn install

FROM nginx:1.9-alpine
WORKDIR /usr/share/nginx/html
COPY index.html /usr/share/nginx/html
COPY --from=deps /app/node_modules .
COPY js/ .
COPY config.json . 